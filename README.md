## Simple Note REST API

### Overview
Spring Boot REST API for Simple Note CRUD

### Prerequisites
* JDK11
* Maven

### Project Dependency & Plugins
* [Spring Boot Starter (Web / Data JPA / Validation / Test)](https://spring.io/projects/spring-boot)
* [H2 Database](https://www.h2database.com)
* [Lombok](https://projectlombok.org/) 
* Spring Boot Maven plugin

### Getting Started
1. create artifact jar file
```sh
$ mvn install
```
or
```sh
$ mvn clean install
```
2. execute unit tests
```sh
$ mvn test
```

3. start application
```sh
$ mvn spring-boot:run
```

&nbsp;
### Create deployment file
1. create deployment jar file, artifact will be created in 'deploy' folder

```sh
$ mvn install
```

or

```sh
$ mvn clean install
```

2. run executable jar file

```sh
$ java -jar simple-note-rest-api.jar
```

&nbsp;
### API Component Design

| Class                         | Layer             | Responsibility                                                                            |
|-------------------------------|:------------------|:------------------------------------------------------------------------------------------|
| SimpleNoteController          | controller        | receive, validate request, and pass data for business process processing                  |
| SimpleNoteService             | business service  | process business logics </br> throw abstract application exception when an error occurred |
| SimpleNoteRepository          | repository        | handle data manipulation between service and database                                     |
| SimpleNote                    | data model        | store request and response data (also ORM to database table)                              |
| SimpleNoteExceptionController | controller advice | handle exception thrown from controller processing                                        |

&nbsp;
### API Usage

|  #  | Description               | Request Path       | HTTP Method | Request Body | HTTP Status | Response Body |
|:---:|:--------------------------|:-------------------|:-----------:|:------------:|:-----------:|:-------------:|
|  1  | Find all simple notes     | /simple-notes      |     GET     |      No      |     200     |      Yes      |
|  2  | Find exists simple note   | /simple-notes/{id} |     GET     |      No      |     200     |      Yes      |
|  3  | Create new simple note    | /simple-notes      |    POST     |     Yes      |     201     |      Yes      |
|  4  | Update exists simple note | /simple-notes/{id} |     PUT     |     Yes      |     200     |      Yes      |
|  5  | Delete exists simple note | /simple-notes/{id} |   DELETE    |      No      |     200     |      No       |

> **Remarks:**
> * All request and response bodies will be filled in data wrapper object 
> * If simple note is not found **(#2, #3, #5)**, HTTP status 204 will be return without response body 
> * HTTP status 500 without response body will be returned if unknown internal error occurred

&nbsp;
### Simple Note Data Model

|  #  | Field    | Description                          | Data type |   Format / Length   | Nullable in Request Body |
|:---:|:---------|:-------------------------------------|:---------:|:-------------------:|:------------------------:|
|  1  | id       | generated id of a simple note posted |  integer  |          -          |       ALWAYS NULL        |
|  2  | story    | simple note story                    |  string   |    Text / 0-500     |          UPDATE          |
|  3  | detail   | note detail                          |  string   |    Text / 0-500     |      CREATE/UPDATE       |
|  4  | dateTime | timestamp simple note posted         |  string   |    ISO Date Time    |          UPDATE          |

&nbsp;
### Sample Request & Response Body
Request body for creating **(#1)** and updating **(#2)** simple note, **no id required**

```
{
    "data": {
        "story": "Merry Christmas",
        "detail": "We wish you a merry christmas",
        "dateTime": "2021-12-24T21:00:01"
    }
}
```

\
Response body after created **(#1)** and updated **(#2)** simple note

```
{
    "data": {
        "id": 1,
        "story": "Merry Christmas",
        "detail": "We wish you a merry christmas",
        "dateTime": "2021-12-24T21:00:01"
    }
}
```

\
Response body from finding all simple notes **(#3)**
```
{
    "data": [
        {
            "id": 1,
            "story": "Merry Christmas",
            "detail": "We wish you a merry christmas",
            "dateTime": "2021-12-24T21:00:01"
        },
        {
            "id": 2,
            "story": "Goodbye last year",
            "detail": "It was a good year",
            "dateTime": "2021-12-31T23:59:59"
        }
    ]
}
```

\
Response body from finding exist simple note by id **(#4)**

```
{
    "data": {
        "id": 1,
        "story": "Merry Christmas",
        "detail": "We wish you a merry christmas",
        "dateTime": "2021-12-24T21:00:01"
    }
}
```

&nbsp;
### Contact
Wasun Kaewpradub (SUN) - [wasun.kaewpradub@gmail.com](mailto:wasun.kaewpradub@gmail.com)

