package kpd.wasun.simplenote.controller;

import kpd.wasun.simplenote.entity.SimpleNote;
import kpd.wasun.simplenote.exception.AbstractApplicationException;
import kpd.wasun.simplenote.model.DataWrapper;
import kpd.wasun.simplenote.model.SimpleNoteCreateValidationGroup;
import kpd.wasun.simplenote.model.SimpleNoteUpdateValidationGroup;
import kpd.wasun.simplenote.service.SimpleNoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/simple-notes")
public class SimpleNoteController {

    private final SimpleNoteService simpleNoteService;

    @GetMapping("/{id}")
    public ResponseEntity<DataWrapper<SimpleNote>> findById(@PathVariable Long id) throws AbstractApplicationException {
        var simpleNote = simpleNoteService.findById(id);
        return ResponseEntity.ok(new DataWrapper<>(simpleNote));
    }

    @GetMapping
    public ResponseEntity<DataWrapper<List<SimpleNote>>> findAll() {
        var simpleNotes = simpleNoteService.findAll();
        return ResponseEntity.ok(new DataWrapper<>(simpleNotes));
    }

    @PostMapping
    public ResponseEntity<DataWrapper<SimpleNote>> create(@Validated(SimpleNoteCreateValidationGroup.class)
                                                          @RequestBody DataWrapper<SimpleNote> dataWrapper) {

        var createdSimpleNote = simpleNoteService.create(dataWrapper.getData());
        return ResponseEntity.status(HttpStatus.CREATED).body(new DataWrapper<>(createdSimpleNote));
    }

    @PutMapping("/{id}")
    public ResponseEntity<DataWrapper<SimpleNote>> update(@PathVariable Long id,
                                                          @Validated(SimpleNoteUpdateValidationGroup.class)
                                                          @RequestBody DataWrapper<SimpleNote> dataWrapper)
            throws AbstractApplicationException {

        var updatedSimpleNote = simpleNoteService.update(id, dataWrapper.getData());
        return ResponseEntity.ok().body(new DataWrapper<>(updatedSimpleNote));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DataWrapper<SimpleNote>> deleteById(@PathVariable Long id) throws AbstractApplicationException {
        simpleNoteService.deleteById(id);
        return ResponseEntity.ok().body(null);
    }
}
