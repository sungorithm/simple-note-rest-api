package kpd.wasun.simplenote.controller.advice;

import kpd.wasun.simplenote.controller.SimpleNoteController;
import kpd.wasun.simplenote.exception.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Slf4j
@RestControllerAdvice(assignableTypes = SimpleNoteController.class)
public class SimpleNoteExceptionController {

    @ExceptionHandler({
            HttpMessageNotReadableException.class,
            MethodArgumentNotValidException.class,
            MethodArgumentTypeMismatchException.class,
    })
    public ResponseEntity<Object> handleInvalidRequest(Exception e) {
        log.error("Bad request", e);
        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(ItemNotFoundException.class)
    public ResponseEntity<Object> handleItemNotFound() {
        log.error("Item not found");
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleUnknownError(Exception e) {
        log.error("Unknown internal error", e);
        return ResponseEntity.internalServerError().build();
    }
}
