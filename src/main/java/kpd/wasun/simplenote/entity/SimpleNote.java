package kpd.wasun.simplenote.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import kpd.wasun.simplenote.model.SimpleNoteCreateValidationGroup;
import kpd.wasun.simplenote.model.SimpleNoteUpdateValidationGroup;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)

@Entity
@Table(name = "simple_note")
public class SimpleNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {
            SimpleNoteCreateValidationGroup.class,
            SimpleNoteUpdateValidationGroup.class
    })
    private Long id;

    @NotBlank(groups = SimpleNoteCreateValidationGroup.class)
    @Size(min = 1, max = 50, groups = SimpleNoteCreateValidationGroup.class)
    @Size(max = 50, groups = SimpleNoteUpdateValidationGroup.class)
    private String story;

    @Size(max = 500, groups = {
            SimpleNoteCreateValidationGroup.class,
            SimpleNoteUpdateValidationGroup.class
    })
    private String detail;

    @NotNull(groups = SimpleNoteCreateValidationGroup.class)
    private LocalDateTime dateTime;
}
