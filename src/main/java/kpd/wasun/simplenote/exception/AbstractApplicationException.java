package kpd.wasun.simplenote.exception;

public abstract class AbstractApplicationException extends Exception {

    public AbstractApplicationException(String message) {
        super(message);
    }

    public AbstractApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
