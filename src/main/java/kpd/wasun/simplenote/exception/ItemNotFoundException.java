package kpd.wasun.simplenote.exception;

public class ItemNotFoundException extends AbstractApplicationException {

    public ItemNotFoundException() {
        super("Item not found");
    }
}
