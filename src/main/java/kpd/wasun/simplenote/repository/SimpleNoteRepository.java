package kpd.wasun.simplenote.repository;

import kpd.wasun.simplenote.entity.SimpleNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SimpleNoteRepository extends JpaRepository<SimpleNote, Long> {

}
