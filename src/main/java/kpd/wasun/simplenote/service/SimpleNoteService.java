package kpd.wasun.simplenote.service;

import kpd.wasun.simplenote.entity.SimpleNote;
import kpd.wasun.simplenote.exception.AbstractApplicationException;

import java.util.List;

public interface SimpleNoteService {

    SimpleNote findById(Long id) throws AbstractApplicationException;

    List<SimpleNote> findAll();

    SimpleNote create(SimpleNote simpleNote);

    SimpleNote update(Long id, SimpleNote simpleNote) throws AbstractApplicationException;

    void deleteById(Long id) throws AbstractApplicationException;
}
