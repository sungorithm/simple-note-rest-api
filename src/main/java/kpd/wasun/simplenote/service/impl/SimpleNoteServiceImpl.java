package kpd.wasun.simplenote.service.impl;

import kpd.wasun.simplenote.entity.SimpleNote;
import kpd.wasun.simplenote.exception.AbstractApplicationException;
import kpd.wasun.simplenote.exception.ItemNotFoundException;
import kpd.wasun.simplenote.repository.SimpleNoteRepository;
import kpd.wasun.simplenote.service.SimpleNoteService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SimpleNoteServiceImpl implements SimpleNoteService {

    private final SimpleNoteRepository simpleNoteRepository;

    @Override
    public SimpleNote findById(Long id) throws AbstractApplicationException {
        return simpleNoteRepository.findById(id)
                .orElseThrow(ItemNotFoundException::new);
    }

    @Override
    public List<SimpleNote> findAll() {
        return simpleNoteRepository.findAll();
    }

    @Override
    public SimpleNote create(SimpleNote simpleNote) {
        return simpleNoteRepository.save(simpleNote);
    }

    @Override
    public SimpleNote update(Long id, SimpleNote simpleNote) throws AbstractApplicationException {
        var existsSimpleNote = simpleNoteRepository.findById(id)
                .orElseThrow(ItemNotFoundException::new);

        if (simpleNote.getStory() != null && !simpleNote.getStory().isBlank()) {
            existsSimpleNote.setStory(simpleNote.getStory());
        }

        if (simpleNote.getDetail() != null && !simpleNote.getDetail().isBlank()) {
            existsSimpleNote.setDetail(simpleNote.getDetail());
        }

        if (simpleNote.getDateTime() != null) {
            existsSimpleNote.setDateTime(simpleNote.getDateTime());
        }

        return simpleNoteRepository.save(existsSimpleNote);
    }

    @Override
    public void deleteById(Long id) throws AbstractApplicationException {
        try {
            simpleNoteRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new ItemNotFoundException();
        }
    }
}
