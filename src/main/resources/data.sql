create table if not exists simple_note (

    id          int             not null    auto_increment,
    story       varchar(50)     not null,
    detail      varchar(500)    not null,
    date_time   timestamp       not null,

    primary key (id)
);