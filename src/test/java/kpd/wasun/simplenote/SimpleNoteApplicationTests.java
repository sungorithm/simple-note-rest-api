package kpd.wasun.simplenote;

import kpd.wasun.simplenote.controller.HealthController;
import kpd.wasun.simplenote.controller.SimpleNoteController;
import kpd.wasun.simplenote.repository.SimpleNoteRepository;
import kpd.wasun.simplenote.service.SimpleNoteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class SimpleNoteApplicationTests {

    @Autowired
    private HealthController healthController;

    @Autowired
    private SimpleNoteController simpleNoteController;

    @Autowired
    private SimpleNoteService simpleNoteService;

    @Autowired
    private SimpleNoteRepository simpleNoteRepository;

    @Test
    void contextLoads() {
        assertNotNull(healthController);
        assertNotNull(simpleNoteController);
        assertNotNull(simpleNoteService);
        assertNotNull(simpleNoteRepository);
    }
}
