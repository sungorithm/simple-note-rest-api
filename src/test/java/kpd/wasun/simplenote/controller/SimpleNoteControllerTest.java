package kpd.wasun.simplenote.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kpd.wasun.simplenote.entity.SimpleNote;
import kpd.wasun.simplenote.exception.ItemNotFoundException;
import kpd.wasun.simplenote.model.DataWrapper;
import kpd.wasun.simplenote.service.SimpleNoteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SimpleNoteController.class)
class SimpleNoteControllerTest {

    private LocalDateTime currentDateTime;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SimpleNoteService simpleNoteService;

    @BeforeEach
    void setUp() {
        currentDateTime = LocalDateTime.now();
    }

    @Test
    void injectedNotNull() {
        assertNotNull(simpleNoteService);
    }

    private SimpleNote createSimpleNote(Long id) {
        var simpleNote = SimpleNote.builder()
                .story("Story name")
                .detail("Story detail")
                .dateTime(currentDateTime)
                .build();

        if (id != null) {
            simpleNote.setId(id);
        }

        return simpleNote;
    }

    @Test
    void findById_givenExistsId_thenReturnSimpleNoteData() throws Exception {
        var id = 1L;
        when(simpleNoteService.findById(id)).thenReturn(createSimpleNote(id));

        var mvcResult = mockMvc.perform(get("/simple-notes/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        var expectedData = new DataWrapper<>(createSimpleNote(id));

        var respBody = mvcResult.getResponse().getContentAsString();
        var typeRef = new TypeReference<DataWrapper<SimpleNote>>() {
        };
        var actualResp = objectMapper.readValue(respBody, typeRef);

        assertEquals(expectedData, actualResp);
        verify(simpleNoteService, times(1)).findById(id);
    }

    @Test
    void findById_givenExistsId_thenReturnHttp204() throws Exception {
        var id = 0L;
        doThrow(ItemNotFoundException.class).when(simpleNoteService).findById(id);

        mockMvc.perform(get("/simple-notes/0"))
                .andExpect(status().isNoContent());

        verify(simpleNoteService, times(1)).findById(id);
    }

    @Test
    void findAll_thenReturnSimpleNoteList() throws Exception {
        when(simpleNoteService.findAll()).thenReturn(
                Arrays.asList(
                        createSimpleNote(1L),
                        createSimpleNote(2L),
                        createSimpleNote(3L)
                )
        );

        var mvcResult = mockMvc.perform(get("/simple-notes"))
                .andExpect(status().isOk())
                .andReturn();

        var expectedData = new DataWrapper<>(
                Arrays.asList(
                        createSimpleNote(1L),
                        createSimpleNote(2L),
                        createSimpleNote(3L)
                )
        );

        var respBody = mvcResult.getResponse().getContentAsString();
        var typeRef = new TypeReference<DataWrapper<List<SimpleNote>>>() {
        };
        var actualData = objectMapper.readValue(respBody, typeRef);

        assertEquals(expectedData, actualData);
        verify(simpleNoteService, times(1)).findAll();
    }

    @Test
    void create_givenSimpleNote_thenReturnSimpleNote() throws Exception {
        var inputSimpleNote = createSimpleNote(null);
        var outputSimpleNote = createSimpleNote(1L);

        when(simpleNoteService.create(inputSimpleNote)).thenReturn(outputSimpleNote);

        var reqData = new DataWrapper<>(createSimpleNote(null));
        var mvcResult = mockMvc.perform(post("/simple-notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reqData))
                )
                .andExpect(status().isCreated())
                .andReturn();

        var expectedData = new DataWrapper<>(createSimpleNote(1L));

        var respBody = mvcResult.getResponse().getContentAsString();
        var typeRef = new TypeReference<DataWrapper<SimpleNote>>() {
        };
        var actualData = objectMapper.readValue(respBody, typeRef);

        assertEquals(expectedData, actualData);
        verify(simpleNoteService, times(1)).create(inputSimpleNote);
    }

    @ParameterizedTest
    @CsvSource({
            "1,story,detail,2021-06-06T10:00:00",
            ",,detail,2021-06-06T10:00:00",
            ",story,detail,",
    })
    void create_givenInvalidSimpleNote_thenReturnHttp400(Long id, String story, String detail, LocalDateTime dateTime)
            throws Exception {

        var invalidSimpleNote = SimpleNote.builder()
                .id(id)
                .story(story)
                .detail(detail)
                .dateTime(dateTime)
                .build();

        var reqData = new DataWrapper<>(invalidSimpleNote);
        mockMvc.perform(post("/simple-notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reqData))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    void update_givenExistsAndSimpleNote_thenReturnSimpleNoteData() throws Exception {
        var id = 1L;
        var updatingSimpleNote = createSimpleNote(null);
        var updatedSimpleNote = createSimpleNote(id);

        when(simpleNoteService.update(id, updatingSimpleNote)).thenReturn(updatedSimpleNote);

        var reqData = new DataWrapper<>(createSimpleNote(null));
        var mvcResult = mockMvc.perform(put("/simple-notes/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reqData))
                )
                .andExpect(status().isOk())
                .andReturn();

        var expectedData = new DataWrapper<>(createSimpleNote(id));

        var respBody = mvcResult.getResponse().getContentAsString();
        var typeRef = new TypeReference<DataWrapper<SimpleNote>>() {
        };
        var actualData = objectMapper.readValue(respBody, typeRef);

        assertEquals(expectedData, actualData);
        verify(simpleNoteService, times(1)).update(id, updatingSimpleNote);
    }

    @Test
    void update_givenNotExistsAndSimpleNote_thenReturnHttp204() throws Exception {
        var id = 0L;
        var updatingSimpleNote = createSimpleNote(null);
        doThrow(ItemNotFoundException.class).when(simpleNoteService).update(id, updatingSimpleNote);

        var reqData = new DataWrapper<>(createSimpleNote(null));

        mockMvc.perform(put("/simple-notes/0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reqData))
                )
                .andExpect(status().isNoContent())
                .andExpect(content().bytes(new byte[0]));

        verify(simpleNoteService, times(1)).update(id, updatingSimpleNote);
    }

    @ParameterizedTest
    @CsvSource({
            "1,story,detail,2021-06-06T10:00:00",
            ",,detail,2021-06-06T10:00:00",
            ",story,detail,",
    })
    void update_givenInvalidSimpleNote_thenReturnHttp400(Long id, String story, String detail, LocalDateTime dateTime)
            throws Exception {

        var invalidSimpleNote = SimpleNote.builder()
                .id(id)
                .story(story)
                .detail(detail)
                .dateTime(dateTime)
                .build();

        var reqData = new DataWrapper<>(invalidSimpleNote);
        mockMvc.perform(post("/simple-notes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reqData))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().bytes(new byte[0]));
    }

    @Test
    void deleteById_givenExistsId_thenReturnHttp200() throws Exception {
        var id = 1L;
        doNothing().when(simpleNoteService).deleteById(id);

        mockMvc.perform(delete("/simple-notes/1"))
                .andExpect(status().isOk())
                .andExpect(content().bytes(new byte[0]));

        verify(simpleNoteService, times(1)).deleteById(id);
    }

    @Test
    void deleteById_givenNotExistsId_thenReturnHttp204() throws Exception {
        var id = 0L;
        doThrow(ItemNotFoundException.class).when(simpleNoteService).deleteById(id);

        mockMvc.perform(delete("/simple-notes/0"))
                .andExpect(status().isNoContent())
                .andExpect(content().bytes(new byte[0]));

        verify(simpleNoteService, times(1)).deleteById(id);
    }
}