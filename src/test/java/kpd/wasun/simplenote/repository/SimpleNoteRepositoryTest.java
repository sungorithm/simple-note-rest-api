package kpd.wasun.simplenote.repository;

import kpd.wasun.simplenote.entity.SimpleNote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class SimpleNoteRepositoryTest {

    private LocalDateTime currentDateTime;

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private SimpleNoteRepository simpleNoteRepository;

    @BeforeEach
    void setUp() {
        currentDateTime = LocalDateTime.now();
    }

    private SimpleNote createSimpleNote(Long id) {
        var simpleNote = SimpleNote.builder()
                .story("Story name")
                .detail("Story detail")
                .dateTime(currentDateTime)
                .build();

        if (id != null) {
            simpleNote.setId(id);
        }

        return simpleNote;
    }

    @Test
    void findById_givenExistsId_thenReturnPresentSimpleNoteOptional() {
        this.testEntityManager.persist(createSimpleNote(null));

        var expectedSimpleNote = createSimpleNote(1L);
        var actualSimpleNoteOpt = simpleNoteRepository.findById(1L);

        assertTrue(actualSimpleNoteOpt.isPresent());
        assertEquals(expectedSimpleNote, actualSimpleNoteOpt.get());
    }

    @Test
    void findById_givenNotExistsId_thenReturnNotPresentSimpleNoteOptional() {
        var actualSimpleNoteOpt = simpleNoteRepository.findById(0L);
        assertTrue(actualSimpleNoteOpt.isEmpty());
    }

    @Test
    void findAll_thenReturnSimpleNotes() {
        this.testEntityManager.persist(createSimpleNote(null));
        this.testEntityManager.persist(createSimpleNote(null));
        this.testEntityManager.persist(createSimpleNote(null));

        var expectedSimpleNotes = Arrays.asList(
                createSimpleNote(1L),
                createSimpleNote(2L),
                createSimpleNote(3L)
        );

        var actualSimpleNotes = simpleNoteRepository.findAll();
        assertEquals(expectedSimpleNotes, actualSimpleNotes);
    }

    @Test
    void save_givenSimpleNote_thenReturnSimpleNote() {
        var expectedSimpleNote = createSimpleNote(1L);
        var actualSimpleNote = simpleNoteRepository.save(createSimpleNote(null));

        assertEquals(expectedSimpleNote, actualSimpleNote);
    }

    @Test
    void deleteById_givenExistsId_thenNotThrowException() {
        this.testEntityManager.persist(createSimpleNote(null));
        assertDoesNotThrow(() -> simpleNoteRepository.deleteById(1L));
    }

    @Test
    void deleteById_givenNotExistsId_thenThrowException() {
        assertThrows(EmptyResultDataAccessException.class, () -> simpleNoteRepository.deleteById(1L));
    }
}