package kpd.wasun.simplenote.service;

import kpd.wasun.simplenote.entity.SimpleNote;
import kpd.wasun.simplenote.exception.AbstractApplicationException;
import kpd.wasun.simplenote.exception.ItemNotFoundException;
import kpd.wasun.simplenote.repository.SimpleNoteRepository;
import kpd.wasun.simplenote.service.impl.SimpleNoteServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SimpleNoteServiceTest {

    private SimpleNoteService simpleNoteService;
    private LocalDateTime currentDateTime;

    @Mock
    private SimpleNoteRepository simpleNoteRepository;

    @BeforeEach
    void setUp() {
        simpleNoteService = new SimpleNoteServiceImpl(simpleNoteRepository);
        currentDateTime = LocalDateTime.now();
    }

    @Test
    void injectedNotNull() {
        assertNotNull(simpleNoteRepository);
        assertNotNull(simpleNoteService);
    }

    private SimpleNote createSimpleNote(Long id) {
        var simpleNote = SimpleNote.builder()
                .story("Story name")
                .detail("Story detail")
                .dateTime(currentDateTime)
                .build();

        if (id != null) {
            simpleNote.setId(id);
        }

        return simpleNote;
    }

    @Test
    void findById_givenExistsId_thenReturnSimpleNote() throws AbstractApplicationException {
        var id = 1L;
        when(simpleNoteRepository.findById(1L)).thenReturn(Optional.of(createSimpleNote(id)));

        var expectedSimpleNote = createSimpleNote(id);
        var actualSimpleNote = simpleNoteService.findById(id);

        assertEquals(expectedSimpleNote, actualSimpleNote);
        verify(simpleNoteRepository, times(1)).findById(id);
    }

    @Test
    void findById_givenNotExistsId_thenThrowItemNotFoundException() {
        var id = 0L;
        when(simpleNoteRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ItemNotFoundException.class, () -> simpleNoteService.findById(id));
        verify(simpleNoteRepository, times(1)).findById(id);
    }

    @Test
    void findAll_thenReturnSimpleNoteList() {
        when(simpleNoteRepository.findAll()).thenReturn(
                Arrays.asList(
                        createSimpleNote(1L),
                        createSimpleNote(2L),
                        createSimpleNote(3L)
                )
        );

        var expectedSimpleNotes = Arrays.asList(
                createSimpleNote(1L),
                createSimpleNote(2L),
                createSimpleNote(3L)
        );

        assertEquals(expectedSimpleNotes, simpleNoteService.findAll());
        verify(simpleNoteRepository, times(1)).findAll();
    }

    @Test
    void create_givenSimpleNote_thenReturnSimpleNote() {
        var id = 1L;
        var simpleNote = createSimpleNote(null);
        var mockSimpleNote = createSimpleNote(id);

        when(simpleNoteRepository.save(simpleNote)).thenReturn(mockSimpleNote);

        var expectedSimpleNote = createSimpleNote(id);
        var actualSimpleNote = simpleNoteService.create(simpleNote);

        assertEquals(expectedSimpleNote, actualSimpleNote);
        verify(simpleNoteRepository, times(1)).save(simpleNote);
    }

    @Test
    void update_givenExistsIdAndSimpleNote_thenReturnSimpleNote() throws Exception {
        var id = 1L;
        var updatedTimestamp = LocalDateTime.now();

        var existsSimpleNote = createSimpleNote(1L);
        var updatedSimpleNote = createSimpleNote(1L);
        updatedSimpleNote.setStory("Updated story");
        updatedSimpleNote.setDetail("Updated detail");
        updatedSimpleNote.setDateTime(updatedTimestamp);

        when(simpleNoteRepository.findById(id)).thenReturn(Optional.of(existsSimpleNote));
        when(simpleNoteRepository.save(updatedSimpleNote)).thenReturn(updatedSimpleNote);

        var expectedSimpleNote = SimpleNote.builder()
                .id(id)
                .story("Updated story")
                .detail("Updated detail")
                .dateTime(updatedTimestamp)
                .build();

        var actualSimpleNote = simpleNoteService.update(id, updatedSimpleNote);

        assertEquals(expectedSimpleNote, actualSimpleNote);
        verify(simpleNoteRepository, times(1)).findById(id);
        verify(simpleNoteRepository, times(1)).save(updatedSimpleNote);
    }

    @Test
    void deleteById_givenExistsId_thenNotThrowException() {
        var id = 1L;
        doNothing().when(simpleNoteRepository).deleteById(id);

        assertDoesNotThrow(() -> simpleNoteService.deleteById(id));
        verify(simpleNoteRepository, times(1)).deleteById(id);
    }

    @Test
    void deleteById_givenNotExistsId_thenThrowItemNotFoundException() {
        var id = 0L;
        doThrow(EmptyResultDataAccessException.class).when(simpleNoteRepository).deleteById(id);

        assertThrows(ItemNotFoundException.class, () -> simpleNoteService.deleteById(id));
        verify(simpleNoteRepository, times(1)).deleteById(id);
    }
}